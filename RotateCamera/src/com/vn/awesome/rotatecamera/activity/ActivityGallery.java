package com.vn.awesome.rotatecamera.activity;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.devsmart.android.ui.HorizontalListView;
import com.vn.awesome.rotatecamera.R;
import com.vn.awesome.rotatecamera.adapter.FilterAdapter;
import com.vn.awesome.rotatecamera.entity.FilterListEntity;
import com.vn.awesome.rotatecamera.utils.GPUImageFilterTools;
import com.vn.awesome.rotatecamera.utils.InitFilter;
import com.vn.awesome.rotatecamera.utils.GPUImageFilterTools.FilterAdjuster;
import com.vn.awesome.rotatecamera.utils.GPUImageFilterTools.OnGpuImageFilterChosenListener;

import jp.co.cyberagent.android.gpuimage.GPUImage.OnPictureSavedListener;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

public class ActivityGallery extends Activity implements
		OnSeekBarChangeListener, OnClickListener, OnPictureSavedListener {

	/*
	 * **********************************************************************
	 * Variables
	 * *********************************************************************
	 */

	private static final int REQUEST_PICK_IMAGE = 1;
	private GPUImageFilter mFilter;
	private FilterAdjuster mFilterAdjuster;
	private GPUImageView mGPUImageView;
	private HorizontalListView listview;
	private FilterAdapter mAdapter;
	private InitFilter initFilter;
	private Bitmap originImage = null;

	/*
	 * **********************************************************************
	 * 
	 * @Override
	 * *********************************************************************
	 */

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		listview = (HorizontalListView) findViewById(R.id.listview);

		mGPUImageView = (GPUImageView) findViewById(R.id.gpuimage);

		findViewById(R.id.button_choose_filter).setOnClickListener(this);
		findViewById(R.id.button_save).setOnClickListener(this);
		findViewById(R.id.rotate).setOnClickListener(this);
		((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);

		selectImage();
	}

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent data) {
		switch (requestCode) {
		case REQUEST_PICK_IMAGE:
			if (resultCode == RESULT_OK) {
				try {
					initFilter = new InitFilter(this, data.getData().toString());
					originImage = MediaStore.Images.Media.getBitmap(
							this.getContentResolver(), data.getData());
					handleImage(data.getData());
					initFilter();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				;
			} else {
				finish();
			}
			break;

		default:
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.button_choose_filter:
			try {
				selectImage();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case R.id.button_save:
			saveImage();
			break;

		case R.id.rotate:
			try {
				rotateImage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;

		default:
			break;
		}

	}

	@Override
	public void onPictureSaved(final Uri uri) {
		Toast.makeText(this, "Saved: " + uri.toString(), Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public void onProgressChanged(final SeekBar seekBar, final int progress,
			final boolean fromUser) {
		if (mFilterAdjuster != null) {
			mFilterAdjuster.adjust(progress);
		}
		mGPUImageView.requestRender();
	}

	@Override
	public void onStartTrackingTouch(final SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(final SeekBar seekBar) {
	}

	/*
	 * **********************************************************************
	 * Functions
	 * *********************************************************************
	 */

	private void initFilter() {
		mAdapter = new FilterAdapter(this, initFilter.initData());
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				listview.setAdapter(mAdapter);

			}
		});
		listview.setOnItemClickListener(new filterClickListener(initFilter
				.initData(), this, new OnGpuImageFilterChosenListener() {

			@Override
			public void onGpuImageFilterChosenListener(
					final GPUImageFilter filter) {
				switchFilterTo(filter);
				mGPUImageView.requestRender();
			}

		}));
	}

	private void saveImage() {
		String fileName = System.currentTimeMillis() + ".jpg";
		mGPUImageView.saveToPictures("camera180", fileName, this);
	}

	private void switchFilterTo(final GPUImageFilter filter) {
		if (mFilter == null
				|| (filter != null && !mFilter.getClass().equals(
						filter.getClass()))) {
			mFilter = filter;
			mGPUImageView.setFilter(mFilter);
			mFilterAdjuster = new FilterAdjuster(mFilter);
		}
	}

	public void simpleError(String msg) throws Exception {

		Builder builder = new Builder(this);
		builder.setMessage(msg);

		builder.setCancelable(true);
		builder.setNeutralButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.show();
	}

	private void handleImage(final Uri selectedImage) {
		mGPUImageView.setImage(selectedImage);
	}

	private void selectImage() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_PICK_IMAGE);
	}

	private void rotateImage() throws IOException {
		Toast.makeText(this, "rotate", Toast.LENGTH_LONG).show();
		mGPUImageView.setImage(RotateBitmap(originImage, 180));
	}

	private static Bitmap RotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	/*
	 * **********************************************************************
	 * Inner Class
	 * *********************************************************************
	 */

	private class filterClickListener implements OnItemClickListener {
		private FilterListEntity filters;
		private Context context;
		private OnGpuImageFilterChosenListener listener;

		public filterClickListener(FilterListEntity filters, Context context,
				final OnGpuImageFilterChosenListener listener) {
			this.filters = filters;
			this.context = context;
			this.listener = listener;
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int pos,
				long arg3) {

			listener.onGpuImageFilterChosenListener(GPUImageFilterTools
					.createFilterForType(context, filters.filters.get(pos)));
		}
	}
}
