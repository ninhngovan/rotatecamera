package com.vn.awesome.rotatecamera.adapter;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

import com.vn.awesome.rotatecamera.R;
import com.vn.awesome.rotatecamera.entity.FilterListEntity;
import com.vn.awesome.rotatecamera.utils.GPUImageFilterTools;
import com.vn.awesome.rotatecamera.utils.GPUImageFilterTools.FilterAdjuster;
import com.vn.awesome.rotatecamera.utils.GPUImageFilterTools.OnGpuImageFilterChosenListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FilterAdapter extends BaseAdapter {
	private Context context = null;
	private FilterListEntity filterList = null;
	private GPUImageFilter mFilter;
	private FilterAdjuster mFilterAdjuster;

	public FilterAdapter(Context context, FilterListEntity filterList) {
		this.context = context;
		this.filterList = filterList;
	}

	@Override
	public int getCount() {
		return filterList.names.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			// Variables
			ViewHolder viewHolder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.filter_items, null);
				// configure view holder
				viewHolder = new ViewHolder();
				viewHolder.image = (ImageView) convertView
						.findViewById(R.id.imagee);
				viewHolder.gpuImage = (GPUImageView) convertView
						.findViewById(R.id.gpuimage);
				viewHolder.info = (TextView) convertView
						.findViewById(R.id.title);

				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			// fill data
			// Bitmap icon =
			// BitmapFactory.decodeResource(context.getResources(),
			// filterList.resource.get(position));
			// viewHolder.gpuImage.setImage(icon);
			// viewHolder.gpuImage
			// .setImage(Uri.parse(filterList.uri.get(position)));
			// setfilter(new GpuImageFilter(viewHolder.gpuImage), position);
			viewHolder.image
					.setImageResource(filterList.resource.get(position));
			viewHolder.gpuImage.setVisibility(View.GONE);
			viewHolder.info.setText(filterList.names.get(position));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// View retval = LayoutInflater.from(parent.getContext()).inflate(
		// R.layout.filter_items, null);
		// image = (GPUImageView) retval.findViewById(R.id.image);
		// TextView title = (TextView) retval.findViewById(R.id.title);
		//
		// Uri myUri = Uri.parse(filterList.uri.get(position));
		// image.setImage(myUri);
		// setfilter(new OnGpuImageFilterChosenListener() {
		//
		// @Override
		// public void onGpuImageFilterChosenListener(GPUImageFilter filter) {
		// switchFilterTo(filter);
		// image.requestRender();
		// }
		// }, position);
		// title.setText(filterList.names.get(position));

		return convertView;
	}

	private void setfilter(OnGpuImageFilterChosenListener listener, int position) {
		listener.onGpuImageFilterChosenListener(GPUImageFilterTools
				.createFilterForType(context, filterList.filters.get(position)));
	}

	private class GpuImageFilter implements OnGpuImageFilterChosenListener {
		private GPUImageView image;

		public GpuImageFilter(GPUImageView image) {
			this.image = image;
		}

		@Override
		public void onGpuImageFilterChosenListener(GPUImageFilter filter) {
			switchFilterTo(filter, image);
			image.requestRender();
		}
	}

	private void switchFilterTo(final GPUImageFilter filter, GPUImageView image) {
		if (mFilter == null
				|| (filter != null && !mFilter.getClass().equals(
						filter.getClass()))) {
			mFilter = filter;
			image.setFilter(mFilter);
			mFilterAdjuster = new FilterAdjuster(mFilter);
		}
	}

	private class ViewHolder {
		public GPUImageView gpuImage;
		public ImageView image;
		public TextView info;
	}
}
