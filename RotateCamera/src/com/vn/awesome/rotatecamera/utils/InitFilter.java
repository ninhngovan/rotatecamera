package com.vn.awesome.rotatecamera.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;

import com.vn.awesome.rotatecamera.R;
import com.vn.awesome.rotatecamera.emum.FilterType;
import com.vn.awesome.rotatecamera.entity.FilterListEntity;

public class InitFilter {
	private Context context;
	private String selectedImag;
	private Handler handler;

	public InitFilter(Context context, String selectedImag) {
		this.context = context;
		this.selectedImag = selectedImag;
		handler = new Handler();
	}

	public FilterListEntity initData() {
		final FilterListEntity filters = new FilterListEntity();
		handler.post(new Runnable() {

			@Override
			public void run() {
				filters.addFilter("Default", selectedImag.toString(),
						FilterType.I_NASHVILLE, R.drawable.filter_icon);
				// filters.addFilter("1977", selectedImag.toString(),
				// FilterType.I_1977, R.drawable.retro);

				filters.addFilter("1977", selectedImag.toString(),
						FilterType.I_1977, R.drawable.filter_icon);
				filters.addFilter("Amaro", selectedImag.toString(),
						FilterType.I_AMARO, R.drawable.filter_icon);
				filters.addFilter("Brannan", selectedImag.toString(),
						FilterType.I_BRANNAN, R.drawable.filter_icon);
				filters.addFilter("Earlybird", selectedImag.toString(),
						FilterType.I_EARLYBIRD, R.drawable.filter_icon);
				filters.addFilter("Hefe", selectedImag.toString(),
						FilterType.I_HEFE, R.drawable.filter_icon);
				filters.addFilter("Hudson", selectedImag.toString(),
						FilterType.I_HUDSON, R.drawable.filter_icon);

				filters.addFilter("Inkwell", selectedImag.toString(),
						FilterType.I_INKWELL, R.drawable.filter_icon);
				filters.addFilter("Lomo", selectedImag.toString(),
						FilterType.I_LOMO, R.drawable.filter_icon);
				filters.addFilter("LordKelvin", selectedImag.toString(),
						FilterType.I_LORDKELVIN, R.drawable.filter_icon);
				filters.addFilter("Nashville", selectedImag.toString(),
						FilterType.I_NASHVILLE, R.drawable.filter_icon);
				// filters.addFilter("Rise", selectedImag.toString(),
				// FilterType.I_NASHVILLE, R.drawable.ic_launcher);

				filters.addFilter("Sierra", selectedImag.toString(),
						FilterType.I_SIERRA, R.drawable.filter_icon);
				filters.addFilter("Sutro", selectedImag.toString(),
						FilterType.I_SUTRO, R.drawable.filter_icon);
				filters.addFilter("Toaster", selectedImag.toString(),
						FilterType.I_TOASTER, R.drawable.filter_icon);
				filters.addFilter("Valencia", selectedImag.toString(),
						FilterType.I_VALENCIA, R.drawable.filter_icon);

				filters.addFilter("Walden", selectedImag.toString(),
						FilterType.I_WALDEN, R.drawable.filter_icon);
				filters.addFilter("Xproll", selectedImag.toString(),
						FilterType.I_XPROII, R.drawable.filter_icon);

				filters.addFilter("Contrast", selectedImag.toString(),
						FilterType.CONTRAST, R.drawable.filter_icon);
				// filters.addFilter("Invert", selectedImag.toString(),
				// FilterType.INVERT, R.drawable.ic_launcher);
				filters.addFilter("Pixelation", selectedImag.toString(),
						FilterType.PIXELATION, R.drawable.filter_icon);
				// filters.addFilter("Hue", selectedImag.toString(),
				// FilterType.HUE, R.drawable.ic_launcher);
				filters.addFilter("Gamma", selectedImag.toString(),
						FilterType.GAMMA, R.drawable.filter_icon);
				// filters.addFilter("Brightness", selectedImag.toString(),
				// FilterType.BRIGHTNESS, R.drawable.ic_launcher);
				filters.addFilter("Sepia", selectedImag.toString(),
						FilterType.SEPIA, R.drawable.filter_icon);
				filters.addFilter("Grayscale", selectedImag.toString(),
						FilterType.GRAYSCALE, R.drawable.filter_icon);
				filters.addFilter("Sharpness", selectedImag.toString(),
						FilterType.SHARPEN, R.drawable.filter_icon);
				// filters.addFilter("Sobel Edge Detection",
				// selectedImag.toString(),
				// FilterType.SOBEL_EDGE_DETECTION, R.drawable.ic_launcher);
				// filters.addFilter("3x3 Convolution", selectedImag.toString(),
				// FilterType.THREE_X_THREE_CONVOLUTION,
				// R.drawable.ic_launcher);
				filters.addFilter("Emboss", selectedImag.toString(),
						FilterType.EMBOSS, R.drawable.filter_icon);
				// filters.addFilter("Posterize", selectedImag.toString(),
				// FilterType.POSTERIZE, R.drawable.ic_launcher);
				// filters.addFilter("Grouped filters", selectedImag.toString(),
				// FilterType.FILTER_GROUP, R.drawable.ic_launcher);
				filters.addFilter("Saturation", selectedImag.toString(),
						FilterType.SATURATION, R.drawable.filter_icon);
				// filters.addFilter("Exposure", selectedImag.toString(),
				// FilterType.EXPOSURE, R.drawable.ic_launcher);
				// filters.addFilter("Highlight Shadow",
				// selectedImag.toString(),
				// FilterType.HIGHLIGHT_SHADOW, R.drawable.ic_launcher);
				filters.addFilter("Monochrome", selectedImag.toString(),
						FilterType.MONOCHROME, R.drawable.filter_icon);
				filters.addFilter("Opacity", selectedImag.toString(),
						FilterType.OPACITY, R.drawable.filter_icon);
				// filters.addFilter("RGB", selectedImag.toString(),
				// FilterType.RGB, R.drawable.ic_launcher);
				// filters.addFilter("White Balance", selectedImag.toString(),
				// FilterType.WHITE_BALANCE, R.drawable.ic_launcher);
				filters.addFilter("Vignette", selectedImag.toString(),
						FilterType.VIGNETTE, R.drawable.filter_icon);
				filters.addFilter("ToneCurve", selectedImag.toString(),
						FilterType.TONE_CURVE, R.drawable.filter_icon);
				// filters.addFilter("Blend (Difference)",
				// selectedImag.toString(), FilterType.BLEND_DIFFERENCE,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Source Over)",
				// selectedImag.toString(), FilterType.BLEND_SOURCE_OVER,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Color Burn)",
				// selectedImag.toString(), FilterType.BLEND_COLOR_BURN,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Color Dodge)",
				// selectedImag.toString(), FilterType.BLEND_COLOR_DODGE,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Darken)", selectedImag.toString(),
				// FilterType.BLEND_DARKEN, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Dissolve)",
				// selectedImag.toString(),
				// FilterType.BLEND_DISSOLVE, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Exclusion)",
				// selectedImag.toString(),
				// FilterType.BLEND_EXCLUSION, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Hard Light)",
				// selectedImag.toString(), FilterType.BLEND_HARD_LIGHT,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Lighten)", selectedImag.toString(),
				// FilterType.BLEND_LIGHTEN, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Add)", selectedImag.toString(),
				// FilterType.BLEND_ADD, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Divide)", selectedImag.toString(),
				// FilterType.BLEND_DIVIDE, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Multiply)",
				// selectedImag.toString(),
				// FilterType.BLEND_MULTIPLY, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Overlay)", selectedImag.toString(),
				// FilterType.BLEND_OVERLAY, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Screen)", selectedImag.toString(),
				// FilterType.BLEND_SCREEN, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Alpha)", selectedImag.toString(),
				// FilterType.BLEND_ALPHA, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Color)", selectedImag.toString(),
				// FilterType.BLEND_COLOR, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Hue)", selectedImag.toString(),
				// FilterType.BLEND_HUE, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Saturation)",
				// selectedImag.toString(), FilterType.BLEND_SATURATION,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Luminosity)",
				// selectedImag.toString(), FilterType.BLEND_LUMINOSITY,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Linear Burn)",
				// selectedImag.toString(), FilterType.BLEND_LINEAR_BURN,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Soft Light)",
				// selectedImag.toString(), FilterType.BLEND_SOFT_LIGHT,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Subtract)",
				// selectedImag.toString(),
				// FilterType.BLEND_SUBTRACT, R.drawable.ic_launcher);
				// filters.addFilter("Blend (Chroma Key)",
				// selectedImag.toString(), FilterType.BLEND_CHROMA_KEY,
				// R.drawable.ic_launcher);
				// filters.addFilter("Blend (Normal)", selectedImag.toString(),
				// FilterType.BLEND_NORMAL, R.drawable.ic_launcher);
				// //
				// filters.addFilter("Lookup (Amatorka)",
				// selectedImag.toString(),
				// FilterType.LOOKUP_AMATORKA, R.drawable.ic_launcher);
			}
		});

		return filters;
	}
}
