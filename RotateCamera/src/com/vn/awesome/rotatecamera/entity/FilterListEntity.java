package com.vn.awesome.rotatecamera.entity;

import java.util.LinkedList;
import java.util.List;

import com.vn.awesome.rotatecamera.emum.FilterType;

public class FilterListEntity {
	public List<String> names = new LinkedList<String>();
	public List<Integer> resource = new LinkedList<Integer>();
	public List<String> uri = new LinkedList<String>();
	public List<FilterType> filters = new LinkedList<FilterType>();

	public void addFilter(final String name, final String image,
			final FilterType filter, int res) {
		names.add(name);
		resource.add(res);
		filters.add(filter);
		uri.add(image);
	}

	// Uri myUri = Uri.parse("http://stackoverflow.com")
}
