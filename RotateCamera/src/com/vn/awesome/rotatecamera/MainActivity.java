package com.vn.awesome.rotatecamera;

import com.vn.awesome.rotatecamera.activity.ActivityCamera;
import com.vn.awesome.rotatecamera.activity.ActivityGallery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {

	private ImageView button_gallery = null;
	private ImageView button_camera = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rotate_activity);

		button_camera = (ImageView) findViewById(R.id.btn_camera);
		button_gallery = (ImageView) findViewById(R.id.btn_gallery);

		button_camera.setOnClickListener(this);
		button_gallery.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btn_camera:
				startActivity(ActivityCamera.class);
				break;
			case R.id.btn_gallery:
				startActivity(ActivityGallery.class);
				break;

			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startActivity(final Class<?> activityClass) {
		startActivity(new Intent(this, activityClass));
	}

}
